#!/usr/bin/perl -w

use strict;
use v5.10;

my $dirname = 'mail';
opendir my($dh), $dirname or die "Couldn't open dir '$dirname': $!";
my @files = readdir $dh;
closedir $dh;

my %mail;

foreach my $file ( @files ) {
	next if $file =~ /^\./;

	open my $fh,'<',"$dirname/$file";
	while (<$fh>) {
		chomp;
		$mail{$_} = undef;
	}
}

open my $fh,'>',"mail.txt";
print $fh "$_\n" foreach sort keys %mail;
close $fh;


