#!/usr/bin/perl -w

use strict;
use Mojo::UserAgent;

my %categories = (
	home_appliances       =>  6, #  (503) Бытовая техника
	holidays_wedding      => 11, # (1011) Всё для праздника и свадьбы
	other                 =>  9, #  (875) Деловые предложения
	animals_plants        => 24, # (2393) Животные, растения
	computers_orgtech     => 34, # (3319) Компьютеры, оргтехника
	building_construction => 60, # (5915) Стройка, ремонт, материалы
	health                =>  9, #  (805) Медицина и красота
	clothes               => 29, # (2844) Одежда, обувь, аксессуары
	extra_job             =>  3, #  (218) Подработка
	communication         => 13, # (1217) Средства связи
	child_market          => 18, # (1759) Товары для детей
	services              => 29, # (2883) Услуги
	entertainment         => 12, # (1173) Хобби, досуг, развлечения
	electronics_foto      => 14, # (1389) Электроника и фото
	);

foreach my $cat (keys %categories) {
	my $cat_path = $cat;
	$cat_path =~ s/_/-/g;
	
	foreach my $page ( 1 .. $categories{$cat} ) {
		my $ua = Mojo::UserAgent->new;
		my $url = "http://do.ngs.ru/categories/$cat/pages/100/page/$page/";
		print "page: $page; category: $cat; $url\n";
		my $tx = $ua->get($url);
		$tx->res->content->asset->move_to("html/$cat_path-page-$page.html");
		$ua = $tx = $url = undef;
	}
}

