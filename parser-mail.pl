#!/usr/bin/perl -w

use strict;
use v5.10;

use Mojo::DOM;
use MIME::Base64;

my %categories = (
	home_appliances       =>  6, #  (503) Бытовая техника
	holidays_wedding      => 11, # (1011) Всё для праздника и свадьбы
	other                 =>  9, #  (875) Деловые предложения
	animals_plants        => 24, # (2393) Животные, растения
	computers_orgtech     => 34, # (3319) Компьютеры, оргтехника
	building_construction => 60, # (5915) Стройка, ремонт, материалы
	health                =>  9, #  (805) Медицина и красота
	clothes               => 29, # (2844) Одежда, обувь, аксессуары
	extra_job             =>  3, #  (218) Подработка
	communication         => 13, # (1217) Средства связи
	child_market          => 18, # (1759) Товары для детей
	services              => 29, # (2883) Услуги
	entertainment         => 12, # (1173) Хобби, досуг, развлечения
	electronics_foto      => 14, # (1389) Электроника и фото
	);

foreach my $cat (keys %categories) {
	$cat =~ s/_/-/g;

	my $dirname = 'html';
	opendir my($dh), $dirname or die "Couldn't open dir '$dirname': $!";
	my @files = readdir $dh;
	closedir $dh;

	my %mail;

	foreach my $file ( @files ) {
		next if $file =~ /^\./;
		next unless $file =~ /^$cat/;
#		say $file;

		my $document = do {
		    local $/ = undef;
		    open my $fh, "<:utf8", "$dirname/$file"
		        or die "could not open $dirname/$file: $!";
		    <$fh>;
		};

		$document =~ s/^[^\n]\n//; # удаляем doctype (что-то с ним не работает ничего)

		my $dom = Mojo::DOM->new( $document );

		$dom->find('a[data-mail]')->each(sub { $mail{decode_base64(decode_base64(shift->{'data-mail'}))} = undef });
	
		$dom = $document = undef;
	}

	open my $fh,'>',"mail/$cat.txt";
	print $fh "$_\n" foreach sort keys %mail;
	close $fh;
#	say $_ foreach sort keys %mail;
	
}


